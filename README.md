# README #

Basic setup for kafka.

### How do I get set up? ###

* Download and untar kafka;
* Start zookeeper

```
#!bash

bin/zookeeper-server-start.sh config/zookeeper.properties
```
* Start kafka

```
#!bash

bin/kafka-server-start.sh config/server.properties
```
* Create topics

```
#!bash

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic my-topic
```