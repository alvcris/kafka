package br.com.example;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.google.common.io.Resources;

/**
 * This program reads messages from my-topic topic.
 */
public class Consumer
{
    @SuppressWarnings("resource")
    public static void main(final String[] args)
            throws IOException
    {
        // and the consumer
        KafkaConsumer<String, String> consumer;
        try (InputStream props = Resources.getResource("consumer.props").openStream())
        {
            final Properties properties = new Properties();
            properties.load(props);
            if (properties.getProperty("group.id") == null)
            {
                properties.setProperty("group.id", "group-" + new Random().nextInt(100000));
            }
            consumer = new KafkaConsumer<>(properties);
        }
        final TopicPartition partition = new TopicPartition("my-topic", 0);
        /*
         * Could also use subscribe method, but in this case no partition will be assigned
         * consumer.subscribe(Arrays.asList("my-topic"));
         */
        consumer.assign(Arrays.asList(partition));

        /*
         * As optional, using the option to seek, so you can specify the where to start reading
         */
        consumer.seek(partition, 1);

        // read records with a short timeout. If we time out, we don't really care.
        System.out.println("Getting records:");
        final ConsumerRecords<String, String> records = consumer.poll(2000);
        if (records.count() > 0)
        {
            for (final ConsumerRecord<String, String> record : records)
            {
                System.out.println(record.toString());
            }
        }
        else
        {
            System.out.println("No records found.");
        }
    }
}
