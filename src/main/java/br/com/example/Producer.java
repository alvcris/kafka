package br.com.example;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.common.io.Resources;

public class Producer
{
    public static void main(final String[] args)
            throws IOException
    {
        // set up the producer
        KafkaProducer<String, String> producer;

        try (InputStream props = Resources.getResource("producer.props").openStream())
        {
            final Properties properties = new Properties();
            properties.load(props);
            producer = new KafkaProducer<>(properties);
        }

        try
        {
            for (int i = 0; i < 1000; i++)
            {
                // send lots of messages
                producer.send(
                        new ProducerRecord<>(
                                "my-topic",
                                0,
                                "",
                                String.format("{\"type\":\"test\", \"t\":%.3f, \"k\":%d}", System.nanoTime() * 1e-9, i)));

                // every so often flush
                if (i % 100 == 0)
                {
                    producer.flush();
                    System.out.println("Sent msg number " + i);
                }
            }
        }
        catch (final Throwable throwable)
        {
            System.out.printf("%s", throwable.getMessage());
        }
        finally
        {
            producer.close();
        }
    }
}
